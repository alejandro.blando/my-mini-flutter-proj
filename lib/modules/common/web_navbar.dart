import 'package:beamer/beamer.dart';
import 'package:calibrar_flutter_project/core/navigation/beam_locations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

class WebNavBar extends PreferredSize {
  WebNavBar({Key? key})
      : super(
          ///height of appbar
          preferredSize: Size(double.infinity, 50.0),
          child: Container(),
          key: key,
        );

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 15, right: 15),
      height: 80.0,
      color: Colors.white,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            'BF BLOGS',
            style: TextStyle(
              fontSize: 24,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(
            width: 35.0,
          ),
          TextButton(
              onPressed: () {
                Beamer.of(context).beamToNamed(Routes.blogRoute);
              },
              child: Text(
                'Blog',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              )),
          SizedBox(
            width: 10.0,
          ),
          TextButton(
              onPressed: () {
                Beamer.of(context).beamToNamed(Routes.graphQLRoute);
              },
              child: Text(
                'GraphQL',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ))
        ],
      ),
    );
  }
}
