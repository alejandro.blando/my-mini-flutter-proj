import 'package:beamer/beamer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:calibrar_flutter_project/core/navigation/beam_locations.dart';

class CustomBottomNavBar extends HookWidget {
  const CustomBottomNavBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 55,
      child: BottomNavigationBar(
          items: [
            BottomNavigationBarItem(
                icon: IconButton(
                  onPressed: (){},
                  icon: Icon(
                    Icons.watch
                  ),

                )
            )
          ],
          //bottom navigation bar on scaffold
          elevation: 2,
          backgroundColor: Colors.blue,

      ),
    );
  }
}
