import 'package:calibrar_flutter_project/models/graphql/character.dart';
import 'package:calibrar_flutter_project/modules/graphql/components/rickMorty_item.dart';
import 'package:calibrar_flutter_project/modules/common/web_navbar.dart';
import 'package:calibrar_flutter_project/providers/character_provider.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class GraphQLHomeWeb extends HookConsumerWidget {
  const GraphQLHomeWeb({Key? key}) : super(key: key);

  final _isLoading = false;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final providerWatcher = ref.watch(characterProvider);
    final providerReader = ref.read(characterProvider.notifier);

    return Scaffold(
      appBar: WebNavBar(),
      body: Center(
        child: SingleChildScrollView(
            child: Container(
                child:  Consumer(builder: (BuildContext context, WidgetRef ref, Widget? child) {
                  return ref.watch(characterInitialProvider).when(
                      data: (List<Character> value){
                        return Wrap(
                          direction: Axis.horizontal,
                          children: value.map((character) => RickMortyItem(character: character)).toList()
                        );
                      },
                      error: (Object object, StackTrace? stackTrace){
                        print(stackTrace.toString());
                        return Text(stackTrace.toString());
                      },
                      loading: (){return CircularProgressIndicator();}
                  );
                },),
            ),
        ),
      ),
    );
  }
}
