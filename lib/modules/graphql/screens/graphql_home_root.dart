import 'package:calibrar_flutter_project/modules/graphql/screens/graphql_home.dart';
import 'package:calibrar_flutter_project/modules/graphql/screens/graphql_home_mobile.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

class GraphQLHomeRoot extends HookWidget {
  const GraphQLHomeRoot({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  GraphQLHomeWeb();
    //kIsWeb ? GraphQLHomeWeb(): GraphQLHomeMobile();
  }
}
