import 'package:calibrar_flutter_project/models/graphql/character.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:responsive_framework/responsive_framework.dart';

class RickMortyItem extends StatelessWidget {
  RickMortyItem({Key? key, required this.character}) : super(key: key);
  final Character character;

  @override
  Widget build(BuildContext context) {
    final responsiveWrapper = ResponsiveWrapper.of(context);
    final constantSpacer = SizedBox(height: 7.0);

    final alive = Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          width: 10.0,
          height: 10.0,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(100),
            color: character.isAlive() ? Colors.green : Colors.red,
          ),
        ),
        SizedBox(
          width: 5.0,
        ),
        Text(
          '${character.status} - ${character.species}',
          style: TextStyle(color: Colors.white),
        ),
      ],
    );

    final info = Container(
      padding: EdgeInsets.all(10),
      alignment: Alignment.centerLeft,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        children: [
          Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Text(
              // <- Header
              character.name.toString(),
              maxLines: 1,
              style: TextStyle(fontSize: 25, color: Colors.white),
            ),
            constantSpacer,
            alive
          ]),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Last Known location',
                style: TextStyle(color: Colors.white54),
              ),
              constantSpacer,
              Text(
                character.location?.name ?? '',
                style: TextStyle(color: Colors.white),
              )
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'First Seen in:',
                style: TextStyle(color: Colors.white54),
              ),
              constantSpacer,
              Text(
                character.episodes?.first.name ?? 'Unknown',
                style: TextStyle(color: Colors.white),
              )
            ],
          )
        ],
      ),
    );

    return ConstrainedBox(
      constraints: BoxConstraints(
        maxHeight: responsiveWrapper.isSmallerThan(TABLET)? 450: 200,
        maxWidth: responsiveWrapper.isSmallerThan(TABLET)? 300: 600,
      ),
      child: Card(
        elevation: 5,
        color: Colors.grey,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10))),
        child: Container(
          child: Center(
            child: ResponsiveRowColumn(
              rowCrossAxisAlignment: CrossAxisAlignment.stretch,
              rowMainAxisSize: MainAxisSize.min,
              columnCrossAxisAlignment: CrossAxisAlignment.stretch,
              columnMainAxisSize: MainAxisSize.min,
              layout: responsiveWrapper.isSmallerThan(TABLET) ?
              ResponsiveRowColumnType.COLUMN:ResponsiveRowColumnType.ROW ,
              children: [
                ResponsiveRowColumnItem(
                  child: Expanded(
                      flex: responsiveWrapper.isSmallerThan(TABLET)? 3: 1,
                      child: FittedBox(
                        clipBehavior: Clip.antiAlias,
                        fit: BoxFit.fill,
                        child: ClipRRect(
                          borderRadius: BorderRadius.only(
                              topRight: responsiveWrapper.isSmallerThan(TABLET)?  Radius.circular(10): Radius.zero  ,
                              topLeft: Radius.circular(10),
                              bottomLeft: responsiveWrapper.isSmallerThan(TABLET)? Radius.zero :Radius.circular(10) ),
                          child: Image.network(character.imageURL.toString()),
                        ),
                      )),
                ),
                ResponsiveRowColumnItem(
                    child:Expanded(
                        flex: responsiveWrapper.isSmallerThan(TABLET)? 2 : 2,
                        child: info
                    )
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}