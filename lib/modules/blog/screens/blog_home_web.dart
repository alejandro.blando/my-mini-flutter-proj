import 'package:calibrar_flutter_project/modules/blog/components/blog_scaffold.dart';
import 'package:calibrar_flutter_project/modules/blog/components/blog_tile.dart';
import 'package:calibrar_flutter_project/modules/blog/components/dialogs/add_dialog.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:responsive_framework/responsive_framework.dart';
import '../../../providers/blog_provider.dart';
import 'package:beamer/beamer.dart';

class BlogHomePageWeb extends HookConsumerWidget {

  void _showDialog(BuildContext context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AddDialog();
        }
    );
  }

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final responsiveWrapper = ResponsiveWrapper.of(context);
    final blogWatcher = ref.watch(blogProvider);

    return BlogScaffold(
      side:  ResponsiveVisibility(
        visible: true,
        hiddenWhen: [Condition.smallerThan(name: TABLET)],
        child: Expanded(
          flex: 3,
          child: FittedBox(
            fit: BoxFit.fitWidth,
            child: Column(children: [
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Colors
                      .blue, // TODO: Change to a Material Color
                ),
                onPressed: () {_showDialog(context);},
                child: Center(
                  child: Row(
                    children: [
                      Icon(Icons.add),
                      Text(
                        'Start a new Topic',
                        style: TextStyle(),
                      )
                    ],
                  ),
                ),
              )
            ]),
          ),
        ),
      ),
      body: Container(
        child: ScrollConfiguration(
          behavior: ScrollConfiguration.of(context)
              .copyWith(scrollbars: false),
          child: ListView.separated(
            separatorBuilder: (BuildContext context, int index) {
              return SizedBox(
                height: 10,
              );
            },
            itemCount: blogWatcher.length,
            itemBuilder: (context, index) {
              return BlogTile(blogWatcher[index]);
            },
          ),
        ),
      )
    );
  }
}
