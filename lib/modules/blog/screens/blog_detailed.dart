import 'package:calibrar_flutter_project/models/blog/post.dart';
import 'package:calibrar_flutter_project/modules/blog/components/blog_scaffold.dart';
import 'package:calibrar_flutter_project/modules/blog/components/detailed_blog_post.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter/material.dart';

class BlogDetailedPage extends HookWidget {
  final Post post;
  const BlogDetailedPage( {Key? key, required this.post}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlogScaffold(
      body: Container(
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: Colors.black12,
          border: Border.all(color: Colors.black),
          borderRadius: BorderRadius.all(Radius.circular(10))
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Flexible(child: ScrollConfiguration(
              behavior: ScrollConfiguration.of(context).copyWith(scrollbars: false),
                child: SingleChildScrollView(child: DetailedBlogPost(post: post))
            )
            )
          ],
        ),
      ),
    );
  }
}
