import 'package:calibrar_flutter_project/modules/blog/screens/blog_home_mobile.dart';
import 'package:calibrar_flutter_project/modules/blog/screens/blog_home_web.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

class BlogHomeRoot extends HookWidget {
  const BlogHomeRoot({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlogHomePageWeb();//kIsWeb ? BlogHomePageWeb():BlogHomeMobile();
  }
}
