import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:calibrar_flutter_project/models/blog/comment.dart';


class CommentWidget extends HookWidget {
  final Comment comment;
  const CommentWidget(this.comment, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    final commentPadding = EdgeInsets.all(10);
    final columnSpacer = SizedBox(height: 10);
    final rowSpacer = SizedBox(width: 20,);
    final commenterAvatar =  CircleAvatar(
      backgroundImage: NetworkImage(comment.user.image ?? ''),
    );
    final textComment = Text( comment.comment,
      style: TextStyle(
        fontSize: 16,
        color: Colors.grey,
      ),
    );
    final commentContainer = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [Text(
            '@${comment.user.userHandle}',
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.bold
            ),
          ),
            Spacer(),
            Text('${comment.getPostDate()}',
            style: TextStyle(fontSize: 12,),)
          ],
        ),
        columnSpacer,
        textComment,
      ],
    );

    return Container(
      padding: commentPadding,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          commenterAvatar,
          rowSpacer,
          Flexible(child: commentContainer)
        ],
      )
    );
  }
}
