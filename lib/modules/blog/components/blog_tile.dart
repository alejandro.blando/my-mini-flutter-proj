import 'package:beamer/beamer.dart';
import 'package:calibrar_flutter_project/core/navigation/beam_locations.dart';
import 'package:calibrar_flutter_project/models/blog/post.dart';
import 'package:calibrar_flutter_project/modules/blog/components/dialogs/delete_dialog.dart';
import 'package:calibrar_flutter_project/providers/blog_provider.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class BlogTile extends HookConsumerWidget {
  final Post _post;
  const BlogTile(this._post, {Key? key}) : super(key: key);

  void _showDialog(BuildContext context, BlogNotifier notifier) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return DeleteDialog(
              yes: () {
                notifier.delete(_post.postID);
              },
              no: () {});
          ;
        });
  }

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    //Notifiers
    final blogNotifier = ref.read(blogProvider.notifier);
    //UI
    final header = Row(
      children: [
        Flexible(
          child: Text(
            _post.title,
            maxLines: 1,
            softWrap: false,
            overflow: TextOverflow.fade,
            style: TextStyle(
              fontSize: 28.0,
              fontWeight: FontWeight.bold,
              color: Colors.black,
            ),
          ),
        ),
      ],
    );

    final double _cardInsidePadding = 20.0;
    final int _votes = 50;

    Widget _content(String text) {
      return Text(
        text,
        overflow: TextOverflow.ellipsis,
        maxLines: 4,
        style: TextStyle(
          height: 1.5,
          fontSize: 14.0,
          color: Colors.grey,
        ),
      );
    }

    Widget _postDetails() {
      return Container(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
              children: [
                CircleAvatar(
                  backgroundColor: Colors.transparent,
                  backgroundImage: AssetImage(
                      'assets/smallNezuko.png'), //TODO: Dapat hindi ganito
                ),
                SizedBox(
                  width: 10.0,
                ),
                Text(
                    'Posted by ${_post.user.userName}' //TODO: Change this to UserName
                    )
              ],
            ),
            Expanded(
              child: Container(
                alignment: Alignment.centerRight,
                child: Text('${_post.getDate()}'),
              ),
            )
          ],
        ),
      );
    }

    Widget _space() => SizedBox(height: 25.0);

    return InkWell(
      onTap: () {
        context.beamToNamed(
            Routes.blogDetailedRoute
                .replaceFirst(Parameters.ID, _post.postID.toString()),
            data: _post);
      }, //TODO: Add here to go to blog_detailed screen,
      child: Card(
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
        child: Padding(
          padding: EdgeInsets.all(_cardInsidePadding),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: [
              Expanded(
                flex: 1,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    IconButton(
                        onPressed: () {}, icon: Icon(Icons.arrow_upward)),
                    Text(_votes.toString()),
                    IconButton(
                        onPressed: () {}, icon: Icon(Icons.arrow_downward)),
                  ],
                ),
              ),
              Expanded(
                flex: 10,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    header,
                    _space(),
                    _content(_post.body),
                    _space(),
                    Divider(
                      thickness: 2.0,
                    ),
                    SizedBox(
                      height: 5.0,
                    ),
                    _postDetails(),
                  ],
                ),
              ),
              Flexible(
                  flex: 1,
                  child: IconButton(
                    icon: Icon(Icons.delete_rounded),
                    onPressed: () => {_showDialog(context, blogNotifier)},
                  ))
            ],
          ),
        ),
      ),
    );
  }
}
