import 'package:calibrar_flutter_project/models/blog/post.dart';
import 'package:calibrar_flutter_project/modules/blog/components/comment.dart';
import 'package:flutter/material.dart';

class DetailedBlogPost extends StatelessWidget {
  final Post post;
  const DetailedBlogPost({Key? key, required this.post}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    //Constants
    final containerRadius = BorderRadius.all(Radius.circular(10));
    final containerPadding = EdgeInsets.all(10);
    //Post Constants
    final titleText = Text(
      post.title,
      textAlign: TextAlign.start,
      style: TextStyle(fontSize: 28, fontWeight: FontWeight.w900),
    );
    final postTopInfo = Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: [
              CircleAvatar(
                backgroundColor: Colors.transparent,
                backgroundImage: NetworkImage(
                    post.user.image ?? ''), //TODO: Dapat hindi ganito
              ),
              SizedBox(
                width: 10,
              ),
              Text('Posted by ${post.user.userName}  ')
            ],
          ),
          Text('${post.getDate()}')
        ]);
    final postContent = Container(
      padding: EdgeInsets.all(15),
      child: Text(
        '${post.body}',
        style: TextStyle(height: 1.5, fontSize: 17, color: Colors.grey),
      ),
    );
    final postInfo = Container(
        decoration:
            BoxDecoration(color: Colors.white, borderRadius: containerRadius),
        padding: containerPadding,
        child: Column(
          children: [
            postTopInfo,
            SizedBox(
              height: 10,
            ),
            titleText,
            postContent,
          ],
        ));

    final commentHeader = Text(
      '${post.comments.length} Comments',
      style: TextStyle(
        fontWeight: FontWeight.bold,
        fontSize: 24
      ),
    );

    final postComments = Flexible(
      child: Container(
        padding: containerPadding,
        decoration:
            BoxDecoration(color: Colors.white, borderRadius: containerRadius),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            commentHeader,
            ListView.separated(
                    shrinkWrap: true,
                    primary: false,
                    separatorBuilder: (BuildContext context, int index) {
                      return Row(
                        children: [
                          SizedBox(
                            height: 10.0,
                          ),
                          Expanded(
                            child: Divider(
                              color: Colors.grey,
                              thickness: 1,
                            ),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                        ],
                      );
                    },
                    itemCount: post.comments.length,
                    itemBuilder: (context, index) {
                      return CommentWidget(post.comments[index]);
                    }),
          ],
        ),
      ),
    );

    //Return
    return Padding(
      padding: containerPadding,
      child: ScrollConfiguration(
        behavior: ScrollConfiguration.of(context).copyWith(scrollbars: false),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            postInfo,
            SizedBox(
              height: 10,
            ),
            postComments
          ],
        ),
      ),
    );
  }
}
