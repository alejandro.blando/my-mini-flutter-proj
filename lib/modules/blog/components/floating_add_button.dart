import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

class FloatingAddButton extends StatelessWidget {
  final VoidCallback? onPressed;
  final Widget label;
  const FloatingAddButton({Key? key,required, required
  this.onPressed, required this.label}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton.extended(
        onPressed: onPressed,
        label: label

    );
  }
}
