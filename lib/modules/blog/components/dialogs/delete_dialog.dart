import 'package:flutter/material.dart';

class DeleteDialog extends StatelessWidget {
  final VoidCallback? yes;
  final VoidCallback? no;

   DeleteDialog({
    Key? key,
    required this.yes,
    required this.no
  }) : super(key: key){}

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('Do you wan to delete this post?'),
      actions: [
        TextButton(
          onPressed: () {
            no?.call();
            Navigator.pop(context);
          },
          child: Text('No'),
        ),
        TextButton(
            onPressed: () {
              yes?.call();
              Navigator.pop(context);
            },
            child: Text('Yes'))
      ],
    );
  }
}
