import 'package:calibrar_flutter_project/models/blog/post.dart';
import 'package:calibrar_flutter_project/models/blog/user.dart';
import 'package:calibrar_flutter_project/providers/blog_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class AddDialog extends HookConsumerWidget {
  AddDialog({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final notifier = ref.read(blogProvider.notifier);
    final TextEditingController titleController = useTextEditingController.fromValue(TextEditingValue.empty);
    final TextEditingController  subtitleController = useTextEditingController.fromValue(TextEditingValue.empty);
    final TextEditingController  contentController = useTextEditingController.fromValue(TextEditingValue.empty);

    return AlertDialog(
      title: Center(
        child: Text(
            'Add Post'
        ),
      ),
      content: ConstrainedBox(
        constraints: BoxConstraints(
          maxWidth: 300,
          maxHeight: 300,
        ),
        child: Container(
          padding: EdgeInsets.all(10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Flexible(
                child: TextFormField(
                  controller: titleController,
                  decoration: InputDecoration(hintText: 'Title'),
                ),
              ),
              Flexible(
                child: TextFormField(
                  controller: subtitleController,
                  decoration: InputDecoration(hintText: 'Subtitle'),
                ),
              ),
              Flexible(
                child: TextFormField(
                  controller: contentController,
                  decoration: InputDecoration(hintText: 'Content'),
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              Expanded(
                  child: ElevatedButton(
                    onPressed: (){
                      notifier.add(
                          Post(
                              user: User.initialUser, //TODO: Dapat si Provider ang nagbibigay nito (Ganyan nalang muna)
                              postID: notifier.state.length + 1 , //TODO: Somehow this is wrong
                              title: titleController.text,
                              subtitle: subtitleController.text,
                              body: contentController.text,
                              date: DateTime.now()
                          )
                        );
                    },
                    child: Text(
                        'Add Post'
                    ),
                  )
              )
            ],
          ),
        ),
      ),
    );
  }
}
