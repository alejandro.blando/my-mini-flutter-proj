import 'package:beamer/beamer.dart';
import 'package:calibrar_flutter_project/core/navigation/beam_locations.dart';
import 'package:calibrar_flutter_project/modules/blog/components/dialogs/add_dialog.dart';
import 'package:calibrar_flutter_project/modules/blog/components/floating_add_button.dart';
import 'package:calibrar_flutter_project/modules/blog/components/user_info.dart';
import 'package:calibrar_flutter_project/modules/common/web_navbar.dart';
import 'package:flutter/material.dart';
import 'package:responsive_framework/responsive_framework.dart';

class BlogScaffold extends StatelessWidget {
  final Widget body;
  final Widget? side;
  const BlogScaffold({Key? key, required this.body, this.side}) : super(key: key);

  void _showDialog(BuildContext context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AddDialog();
        }
    );
  }

  @override
  Widget build(BuildContext context) {
    final responsiveWrapper = ResponsiveWrapper.of(context);

    Widget spacer = SizedBox(
      width: responsiveWrapper.isSmallerThan(TABLET) ? 10: 50 ,);
    final bodyPadding = responsiveWrapper.isSmallerThan(TABLET)
        ? EdgeInsets.only(top: 25, bottom: 20, left: 5, right: 5)
        : EdgeInsets.only(top: 50, bottom: 25, left: 25, right: 25);
    //For Visibility of Floating Action Bar
    final beamerState = Beamer.of(context).currentBeamLocation.state;

    return Scaffold(
        backgroundColor: Colors.white12,
        appBar: WebNavBar(),
        floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
        floatingActionButton: ResponsiveVisibility(
          visible: (beamerState.routeInformation.location == Routes.blogRoute) ,
          hiddenWhen: [Condition.largerThan(name: MOBILE)],
          child: FloatingAddButton(
            label: Text('Start a new Topic'),
            onPressed: () {_showDialog(context);},
          ),
        ),
        body: Center(
            child: Padding(
                padding: bodyPadding,
                child: ConstrainedBox(
                  constraints: BoxConstraints(
                      maxWidth: 1280, minWidth: 650, minHeight: 650),
                    child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          ResponsiveVisibility(
                            hiddenWhen: [Condition.smallerThan(name: TABLET)],
                            child: Flexible(
                              //TODO: Add here navigation Bar for Web
                              flex: 3,
                              child: const UserInfo(),
                            ),
                          ),
                          spacer,
                          Expanded(flex: 10, child: body),
                          spacer,
                          side ?? Container(),
                        ]
                    ),
                )),
        ));
  }
}
