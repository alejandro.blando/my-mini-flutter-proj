import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

PagingController usePaginationController(
    ) {
  return use(_PaginationControllerHook());
}

class _PaginationControllerHook extends Hook<PagingController> {

  const _PaginationControllerHook();

  @override
  _PaginationControllerHookState createState() =>
      _PaginationControllerHookState();
}

class _PaginationControllerHookState
    extends HookState<PagingController, _PaginationControllerHook> {
  late PagingController _pagingController;

  @override
  void initHook() {

  }

  @override
  PagingController build(BuildContext context) => _pagingController;

  @override
  void dispose() => _pagingController.dispose();
}
