import 'package:flutter/material.dart';

const primaryColor = Color(0xff45BEF3);
const secondaryColor = Color(0xff42FDFA);
const tertiaryColor = Color(0xff0A065C);

const scaffoldBackgroundColor = Color(0xff45BEF3);

const primaryLightTextColor = Colors.white;
const primaryDarkTextColor = Colors.black;
const backgroundColor = Colors.white;







