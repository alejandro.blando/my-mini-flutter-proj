import 'dart:async';

import 'package:calibrar_flutter_project/core/config/graphql_instance.dart';
import 'package:calibrar_flutter_project/models/graphql/character.dart';
import 'package:graphql/client.dart';

class CharactersGraphQL {
  String _getCharacters = r'''
  query GetCharacters($page: Int){
    characters(page: $page) {
      info {
        count
        pages
        next
        prev
      }
      results {
        id
        name
        image
        created
        status
        species
        type
        gender
        location{
          id
          name
        }
        origin{
          id
          name
        }
        episode{
          id
          name
          }
      }
    }
  }
  ''';

  Future<List<Character>> getCharacters(int page) async {
    final options =
        QueryOptions(document: gql(_getCharacters), variables: {'page': page});

    final response = await graphQLClient.value.query(options);

    if (!response.hasException){
      final List<Object?> data = response.data!['characters']['results'];
      final values = data.map((character) => Character.fromJson(character as Map<String, dynamic>));
      return values.toList();
    }else{
      return [];
    }

  }
}
