import 'package:beamer/beamer.dart';
import 'package:calibrar_flutter_project/models/blog/post.dart';
import 'package:calibrar_flutter_project/modules/blog/screens/blog_detailed.dart';
import 'package:calibrar_flutter_project/modules/blog/screens/blog_home_root.dart';
import 'package:calibrar_flutter_project/modules/graphql/screens/graphql_home_root.dart';
import 'package:flutter/material.dart';

class Routes{
  static const String homeRoute = '/';
  static const String blogRoute = '/blog';
  static const String graphQLRoute = '/graphql';
  static const String blogDetailedRoute = blogRoute + '/:' + Parameters.ID;
}
class Parameters{
  static const String ID = 'id';
}
class SimpleLocationGenerator {
  static final simpleLocationBuilder = RoutesLocationBuilder(
      routes: {
        Routes.homeRoute : (context, state,data) => BeamPage(
          //TODO: Change this Screen
          key: ValueKey('home'),
          title: 'Home',
            child: Center(
              child: BlogHomeRoot(),
            ),
        ),
        Routes.blogRoute : (context, state,data) => BeamPage(
          key: ValueKey('blog'),
          title: 'Blog',
          child: BlogHomeRoot(),
          type: BeamPageType.fadeTransition
        ),
        Routes.blogDetailedRoute : (context, state, data) {
          // Take the path parameter of interest from BeamState
          final blogID = state.pathParameters[Parameters.ID]!;
          // Collect arbitrary data that persists throughout navigation
          final post = (data as Post);
         return BeamPage(
              key: ValueKey('blog-$blogID'),
              title: 'Blog $blogID ',
              popToNamed: Routes.homeRoute,
              child: BlogDetailedPage(post: post),
              type: BeamPageType.fadeTransition
          );
        },

        Routes.graphQLRoute : (context, state,data) => BeamPage(
          key: ValueKey('graphQL'),
          title: 'GraphQL',
          child: GraphQLHomeRoot(),
          type: BeamPageType.fadeTransition
          ),
      }
  );

}