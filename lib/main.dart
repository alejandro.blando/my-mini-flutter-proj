import 'package:beamer/beamer.dart';
import 'package:calibrar_flutter_project/core/navigation/beam_locations.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:responsive_framework/responsive_framework.dart';

void main() => runApp(ProviderScope(child: MyApp())

    // DevicePreview(
    //     enabled: !kReleaseMode,
    //     builder: (context) => MyApp(), // Wrap your app
    //   ),
);

class MyApp extends StatelessWidget {

  final beamerDelegate = BeamerDelegate(
      locationBuilder: SimpleLocationGenerator.
      simpleLocationBuilder
  );

  @override
  Widget build(BuildContext context) {

    return MaterialApp.router(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        theme: ThemeData.light(),
        darkTheme: ThemeData.light(),
        routeInformationParser: BeamerParser(),
        routerDelegate: beamerDelegate,
        backButtonDispatcher: BeamerBackButtonDispatcher(delegate: beamerDelegate),
        builder: (context, widget) => ResponsiveWrapper.builder(
          BouncingScrollWrapper.builder(context, widget!),
          defaultScale: true,
          breakpoints: [
            const ResponsiveBreakpoint.resize(450, name: MOBILE),
            const ResponsiveBreakpoint.autoScale(900, name: TABLET),
            const ResponsiveBreakpoint.resize(1000, name: DESKTOP),
          ],
          background:  Container(color: const Color(0xFFF5F5F5))),
      /*
        locale: DevicePreview.locale(context),
        builder: DevicePreview.appBuilder,*/
    );
  }
}






