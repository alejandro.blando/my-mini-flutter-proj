import 'package:calibrar_flutter_project/models/blog/comment.dart';
import 'package:calibrar_flutter_project/models/blog/user.dart';
import 'package:intl/intl.dart';
class Post{
  final User user;
  final int postID;
  final String title;
  final String body;
  final String subtitle; // Subtitle is thread name
  final DateTime date;
  List<Comment> comments;

   Post({
    required this.user,
    required this.postID,
    required this.title,
    required this.body,
    required this.subtitle,
    required this.date,
    this.comments = const <Comment>[]
  }){}

  String getDate (){
    return DateFormat('MMM dd, yyyy').format(date);
  }


  static List<Post> GeneratePost = [
    Post(user: User.initialUser, postID: 1, title: 'What does the fox Say?',
        body: 'Guys! So I was in the shower last day and it just popped in my head. What does the fox say? Like really?'
            'How do they sound when they speak, I know about dogs, cats, mouse, cow, etc but fox! Never heard of it.'
            'Anyways if any of you guys have any idea. Let me know in the comments. Thanks in advance.',
        subtitle: 'r/Fox',
        date: DateTime.now(),
      comments: Comment.initialComments
    ),
    Post(user: User.initialUser, postID: 2, title: 'Who was the first guy to land on the Moon',
        body: 'body',
        subtitle: 'subtitle',
        date: DateTime.now(),
        comments: Comment.initialComments
    ),
    Post(user: User.initialUser, postID: 3, title: 'COVID-19 Mega Thread Apr 2020',
        body: 'body',
        subtitle: 'subtitle',
        date: DateTime.now(),
        comments: Comment.initialComments
    ),
  ];
}