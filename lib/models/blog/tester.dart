class Tester{

  String nani;

//<editor-fold desc="Data Methods">

  Tester({
    required this.nani,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is Tester &&
          runtimeType == other.runtimeType &&
          nani == other.nani);

  @override
  int get hashCode => nani.hashCode;

  @override
  String toString() {
    return 'Tester{' + ' nani: $nani,' + '}';
  }

  Tester copyWith({
    String? nani,
  }) {
    return Tester(
      nani: nani ?? this.nani,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'nani': this.nani,
    };
  }

  factory Tester.fromMap(Map<String, dynamic> map) {
    return Tester(
      nani: map['nani'] as String,
    );
  }

//</editor-fold>
}