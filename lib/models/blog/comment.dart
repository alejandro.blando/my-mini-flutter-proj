import 'package:calibrar_flutter_project/models/blog/user.dart';
import 'package:intl/intl.dart';
class Comment{
  User user;
  String comment;
  DateTime date;
  
  Comment(
  {
    required this.date,
    required this.user,
    required this.comment
  }){}
  
  
  static List<Comment> initialComments = [
    Comment(user: User.initialUser, comment: 'Mmmmpphh Mmmphh',date: DateTime.now()),
    Comment(user: User.tanjiroUser, comment: "Don't Worry Big brother will save you now matter what!",date: DateTime.now()),
    Comment(user: User.zenitsuUser, comment: 'I love you Nezuko-chan',date: DateTime.now()),
    Comment(user: User.inosukeUser, comment: 'Where are my acorns UNDERLING 3!!!!! ',date: DateTime.now()),
    Comment(user: User.inosukeUser, comment: 'Where are my acorns UNDERLING 3!!!!! ',date: DateTime.now()),
    Comment(user: User.inosukeUser, comment: 'Where are my acorns UNDERLING 3!!!!! ',date: DateTime.now()),
    Comment(user: User.inosukeUser, comment: 'Where are my acorns UNDERLING 3!!!!! ',date: DateTime.now()),
    Comment(user: User.inosukeUser, comment: 'Where are my acorns UNDERLING 3!!!!! ',date: DateTime.now()),
    Comment(user: User.inosukeUser, comment: 'Where are my acorns UNDERLING 3!!!!! ',date: DateTime.now()),
    Comment(user: User.inosukeUser, comment: 'Where are my acorns UNDERLING 3!!!!! ',date: DateTime.now()),
  ];

  String getPostDate() => DateFormat('MMM dd, yyyy').format(date);
}