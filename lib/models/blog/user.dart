class User{
  String userName;
  String userHandle;
  String? image;

  static User initialUser = User(userName: 'Nezuko', userHandle: 'NezukoKamado',
  image: 'https://i.pinimg.com/564x/24/cd/14/24cd14ff6c42e1d8dcce0f24584ae34b.jpg');
  static User tanjiroUser = User(userName: 'Tanjiro',userHandle: 'TanjiroKamado',
      image: 'https://i.pinimg.com/736x/91/7d/6f/917d6fe868189926002ca0db7e80ccd4.jpg' );
  static User inosukeUser = User(userName: 'Inosuke',userHandle: 'Hashibara',
    image: 'https://i.pinimg.com/236x/33/b3/83/33b383395a23f5ce67024a4107e49b88.jpg'
  );
  static User zenitsuUser = User(userName: 'Zenitsu',userHandle: 'ZenitsuAgatsuma',
    image: 'https://i.pinimg.com/originals/cc/f9/59/ccf959bdebc86345978adbf0f3a24bcc.png'
  );


  User({
    required this.userHandle,
    required this.userName,
    String? image
  }){
    this.image = image;
  }

}

