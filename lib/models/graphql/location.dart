import 'package:calibrar_flutter_project/models/graphql/character.dart';

class Location {

  String? id;
  String name;
  String? type;
  String? dimension;
  List<Character>? residents;
  String? created;

  Location({
    required this.name,
    required this.id,
    String? created,
    String? dimension,
    List<Character>? residents,
    String? type,
  }) {
    this.created = created;
    this.dimension = dimension;
    this.residents = residents;
    this.type = type;
  }

  factory Location.fromJson(Map<String, dynamic> json) {

    return Location(
      id: json['id'] as String? ?? '',
      name: json['name'] as String? ?? '',
    );
  }
}