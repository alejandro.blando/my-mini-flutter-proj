import 'character.dart';

class Episode {
  String? id;
  String name;
  String? airDate;
  List<Character>? characters;
  String? created;
  Episode(
      {
      required this.name,
    }) {}

  factory Episode.fromJson(Map<String, dynamic> json) {
    return Episode(
      name: json['name'] as String? ?? '',
    );
  }
}
