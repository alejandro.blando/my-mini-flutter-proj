import 'package:calibrar_flutter_project/models/graphql/episode.dart';
import 'package:calibrar_flutter_project/models/graphql/location.dart';

final getACharacter = Character(
    id: '1',
    name: 'UmayNess',
    type: 'type',
    status: 'Alive',
    species: 'Human',
    gender: 'gender',
    location: Location(name: 'Greenland', id: '1'),
    origin: Location(name: 'Dogwood', id: '1'),
    imageURL: 'https://rickandmortyapi.com/api/character/avatar/336.jpeg',
    episodes: List.of([Episode(name: 'nani')]),
    createdAt: 'createdAt');

class Character {
  String id;
  String name;
  String status;
  String species;
  String type;
  String gender;
  Location? origin;
  Location? location;
  String? imageURL;
  List<Episode>? episodes;
  String? createdAt;

  Character(
      {required this.id,
      required this.name,
      required this.type,
      required this.status,
      required this.species,
      required this.gender,
      required this.imageURL,
      required this.createdAt,
      Location? location,
      Location? origin,
      List<Episode>? episodes}) {
    this.location = location;
    this.origin = origin;
    this.episodes = episodes;
  }


  factory Character.fromJson(Map<String, dynamic> json) {
    final List<Object?> episode = json['episode'];
    final episodes = episode
        .map((episode) => Episode.fromJson(episode as Map<String, dynamic>))
        .toList();

    final Object? location = json['location'];
    final Object? origin = json['origin'];

    return Character(
      id: json['id'] as String? ?? '',
      name: json['name'] as String? ?? '',
      imageURL: json['image'] as String? ?? '',
      createdAt: json['created'] as String? ?? '',
      status: json['status'] as String? ?? '',
      species: json['species'] as String? ?? '',
      type: json['type'] as String? ?? '',
      gender: json['gender'] as String? ?? '',
      location: Location.fromJson(location as Map<String,dynamic>),
      origin: Location.fromJson(origin as Map<String,dynamic>),
      episodes: episodes,
    );
  }

  bool isAlive() {
    switch (this.status) {
      case 'Alive':
        return true;
      default:
        return false;
    }
  }
}
