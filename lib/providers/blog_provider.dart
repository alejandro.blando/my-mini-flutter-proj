import 'package:calibrar_flutter_project/models/blog/post.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

final blogProvider = StateNotifierProvider<BlogNotifier,List<Post>>((ref) => BlogNotifier());

class BlogNotifier extends StateNotifier<List<Post>>{
  BlogNotifier([List<Post>? posts]) : super(posts ?? Post.GeneratePost);

  void add(Post post){
   state = [...state, post];
  }

  void delete(int id){
    state = state.where((post) => post.postID != id).toList();
  }

}