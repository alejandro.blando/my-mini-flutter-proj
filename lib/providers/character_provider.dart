import 'package:calibrar_flutter_project/core/api/characters_graphql.dart';
import 'package:calibrar_flutter_project/models/graphql/character.dart';
import 'package:calibrar_flutter_project/providers/api_providers.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

// Using FutureProvider
final characterInitialProvider = FutureProvider<List<Character>>((ref) async{
  return await ref.read(apiProvider).characterAPI.getCharacters(0);
});

final characterProvider = StateNotifierProvider<CharacterNotifier,List<Character>? >((ref) =>
    CharacterNotifier(ref.read(apiProvider).characterAPI));

class CharacterNotifier extends StateNotifier<List<Character>?> {
  final CharactersGraphQL? _api;
  CharacterNotifier(this._api,[List<Character>? state,])
      : super(state ?? List<Character>.empty()){
    initialLoadCharacters();
  }
  initialLoadCharacters () async {
    state = await _api?.getCharacters(0);
  }
  Future<List<Character>?> getInitialCharacters(int page) async {
    return _api?.getCharacters(page);
  }
  Future<List<Character>?> getMoreCharacters(int page) async {
    return _api?.getCharacters(page);
  }

}