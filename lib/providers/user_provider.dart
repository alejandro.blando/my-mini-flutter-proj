import 'package:calibrar_flutter_project/models/blog/post.dart';
import 'package:calibrar_flutter_project/models/blog/user.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

final userProvider = StateNotifierProvider<UserNotifier,User>((ref) => UserNotifier());

class UserNotifier extends StateNotifier<User>{
  UserNotifier([User? user]) : super(user ?? User.initialUser);

}