import 'package:calibrar_flutter_project/core/api/characters_graphql.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

final apiProvider = Provider<ApiProvider>((ref) => ApiProvider());

class ApiProvider{
  final characterAPI = CharactersGraphQL();
}